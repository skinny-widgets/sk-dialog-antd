
import { SkDialogImpl }  from '../../sk-dialog/src/impl/sk-dialog-impl.js';

export class AntdSkDialog extends SkDialogImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'dialog';
    }

    get tplFooterPath() {
        if (! this._tplFooterPath) {
            this._tplFooterPath = (this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path'))
                ? `${this.comp.configEl.getAttribute('tpl-path')}/${this.prefix}-sk-${this.suffix}-footer.tpl.html`
                :`/node_modules/sk-${this.comp.cnSuffix}-${this.prefix}/src/${this.prefix}-sk-${this.suffix}-footer.tpl.html`;
        }
        return this._tplFooterPath;
    }

    get dialog() {
        if (! this._dialog) {
            this._dialog = this.comp.el.querySelector(this.comp.dialogTn);
        }
        return this._dialog;
    }

    set dialog(dialog) {
        this._dialog = dialog;
    }

    get body() {
        return this.dialog.querySelector('.ant-modal-body');
    }

    get closeBtn() {
        return this.dialog.querySelector('.ant-modal-close');
    }

    get headerEl() {
        return this.dialog.querySelector('.ant-modal-header');
    }

    get footerEl() {
        return this.dialog.querySelector('.ant-modal-footer');
    }

    get titleEl() {
        if (! this._titleEl) {
            this._titleEl = this.dialog.querySelector('.ant-modal-title');
        }
        return this._titleEl;
    }

    get btnCancelEl() {
        return this.dialog.querySelector('.btn-cancel');
    }

    get btnConfirmEl() {
        return this.dialog.querySelector('.btn-confirm');
    }

    restoreState(state) {
        this.body.innerHTML = '';
        this.body.insertAdjacentHTML('beforeend', state.contentsState);
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    afterRendered() {
        //super.afterRendered();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        if (this.dialog) {
            this.polyfillNativeDialog(this.dialog);
        }
        this.bindEvents();
        this.close();
        this.renderTitle();
        if (this.comp.type === 'confirm' || this.comp.type === 'action') {
            this.renderFooter();
        } else {
            this.footerEl.style.display = 'none';
        }

        this.bindActions(this.dialog);
        this.mountStyles();
    }

    bindEvents() {
        super.bindEvents();
        this.closeBtn.onclick = function(event) {
            this.close(event);
        }.bind(this);

    }

    unbindEvents() {
        super.unbindEvents();
        this.closeBtn.onclick = null;
    }


}
